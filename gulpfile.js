// *************************************
//
//   Gulpfile
//
// *************************************
// Available tasks:
//   `gulp`
//   `gulp build`
//   `gulp compile:coffee`
//   `gulp compile:sass`
//   `gulp icons`
//   `gulp lint:coffee`
//   `gulp minify:css`
//   `gulp minify:js`
//   `gulp test:css`
//   `gulp test:js`
// *************************************

// -------------------------------------
//   Plugins
// -------------------------------------
// gulp                       : The streaming build system
// gulp-angular-htmlify       : Change ng-attributes to data* for HTML5
// gulp-angular-templatecache : Concatenates and registers AngularJS templates
// gulp-autoprefixer          : Prefix CSS
// gulp-clean                 : Removes files and folders
// gulp-coffee                : Compile CoffeeScript files
// gulp-coffeelint            : Lint your CoffeeScript
// gulp-concat                : Concatenate files
// gulp-gzip                  : Gzip files
// gulp-if                    : Conditionally control the flow of objects
// gulp-jade                  : Compile Jade
// gulp-jadelint              : Lint your Jade
// gulp-jshint                : JavaScript code quality tool
// gulp-less                  : Compile Less
// gulp-load-plugins          : Automatically load Gulp plugins
// gulp-minify-css            : Minify CSS
// gulp-minify-html           : Minify HTML
// gulp-ng-annotate           : Add angularjs dependency injection annotations
// gulp-parker                : Stylesheet analysis tool
// gulp-plumber               : Prevent pipe breaking from errors
// gulp-rename                : Rename files
// gulp-uglify                : Minify JavaScript with UglifyJS
// gulp-util                  : Utility functions
// gulp-watch                 : Watch stream
// del                        : Delete files and folders
// proxy-middleware           : Connect middleware for routing
// run-sequence               : Run a series of dependent Gulp tasks in order
// -------------------------------------

var gulp    = require('gulp');
var del     = require('del');
var proxy   = require('proxy-middleware');
var url     = require('url');
var run     = require('run-sequence');

var plugins = require('gulp-load-plugins')({
    rename: {
        'gulp-angular-htmlify'       : 'htmlify',
        'gulp-angular-templatecache' : 'tplcache',
        'gulp-minify-css'            : 'cssmin',
        'gulp-minify-html'           : 'htmlmin',
        'gulp-ng-annotate'           : 'annotate',
        'gulp-recess'                : 'lesslint'
    }
});

var browserSync = require('browser-sync');
var reload = browserSync.reload;

var gzip_options = {
    threshold: '1kb',
    gzipOptions: {
        level: 9
    }
};

var options = {
    default : {
        tasks : [ 'build', 'watch' ]
    },
    build : {
        tasks       : [ 'lint:index', 'lint:jade', 'lint:less', 'compile:coffee', 'compile:index',
            'compile:jade', 'compile:less', 'compile:libJs', 'compile:libCss', 'fonts', 'json' ],
        destination : 'build/'
    },
    production : {
        tasks       : [ 'build', 'minify:html', 'minify:css', 'minify:js' ],
        destination : 'build/Release/'
    },
    coffee : {
        files       : [ '!./app/**/*_test.coffee', './app/**/*.coffee' ],
        file        : 'app.js',
        destination : 'build/'
    },
    css : {
        file : './build/*.css'
    },
    fonts : {
        files       : [ './assets/material-design-icons/*.eot',
            './assets/material-design-icons/*.ttf',
            './assets/material-design-icons/*.woff',
            './assets/material-design-icons/*.woff2',
            './bower_components/Materialize/dist/font/**/*.eot',
            './bower_components/Materialize/dist/font/**/*.svg',
            './bower_components/Materialize/dist/font/**/*.ttf',
            './bower_components/Materialize/dist/font/**/*.woff',
            './bower_components/Materialize/dist/font/**/*.woff2' ],
        destination : 'build/font/'
    },
    html : {
        file : './build/*.html'
    },
    index : {
        files       : [ './app/index.jade' ],
        destination : 'build/'
    },
    jade : {
        files       : [ '!./app/index.jade', './app/**/*.jade' ],
        file        : 'templates.js',
        destination : 'build/'
    },
    js : {
        file : './build/*.js'
    },
    json : {
        files       : [ './app/search/interface/options/*.json' ],
        destination : 'build/constants/'
    },
    less : {
        files       : [ './app/**/*.less' ],
        file        : 'app.css',
        destination : 'build/'
    },
    libjs: {
        files       : [ './bower_components/jquery/dist/jquery.js',
            './bower_components/angular/angular.js',
            './bower_components/angular-route/angular-route.js',
            './bower_components/angular-sanitize/angular-sanitize.js',
            './bower_components/angular-ui-router/release/angular-ui-router.js',
            './bower_components/Materialize/dist/js/materialize.js',
            './bower_components/angular-materialize/src/angular-materialize.js',
            './bower_components/x2js/xml2json.js',
            './bower_components/angular-x2js/src/x2js.js',
            './node_modules/http-string-parser/lib/parser.js' ],
        file        : 'lib.js',
        destination : 'build/'
    },
    libcss: {
        files       : [ '!./bower_components/**/*.min.css',
            './bower_components/angular/angular-csp.css',
            './bower_components/Materialize/dist/css/materialize.css',
            './bower_components/angular-materialize/css/style.css' ],
        file        : 'lib.css',
        destination : 'build/'
    },
    watch : {
        files : function() {
            return [
                options.coffee.files,
                options.index.files,
                options.jade.files,
                options.less.files
            ]
        },
        run : function() {
            return [
                [ 'lint:coffee', 'compile:coffee' ],
                [ 'lint:index', 'compile:index' ],
                [ 'lint:jade', 'compile:jade', 'fonts', 'json' ],
                [ 'lint:less', 'compile:less' ]
            ]
        }
    }
};

// 'default' should be for development: most common tasks
gulp.task( 'default', options.default.tasks );

// -------------------------------------
//   Builds
// -------------------------------------
gulp.task('clean', function () {
    return del(['build/']);
});

gulp.task( 'build', function() {
    options.build.tasks.forEach( function( task ) {
        gulp.start( task );
    } );
});

gulp.task( 'production', function() {
    options.production.tasks.forEach( function( task ) {
        gulp.start( task );
    })
});

// -------------------------------------
//   Lint tasks
// -------------------------------------
gulp.task( 'lint:coffee', function () {
    gulp.src( options.coffee.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.coffeelint() )
        .pipe( plugins.coffeelint.reporter() );
} );

gulp.task( 'lint:index', function () {
    gulp.src( options.index.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.jadelint() );
} );

gulp.task( 'lint:jade', function () {
    gulp.src( options.jade.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.jadelint() );
} );

gulp.task( 'lint:less', function () {
    gulp.src( options.less.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.lesslint() )
        .pipe( plugins.lesslint.reporter() );
} );

// -------------------------------------
//   Compile tasks
// -------------------------------------
gulp.task( 'compile:coffee', function() {
    // concatenate compiled .coffee files into build/app.js
    gulp.src( options.coffee.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.coffee( { bare: true } ).on( 'error', plugins.util.log ) )
        .pipe( plugins.concat( options.coffee.file ) )
        .pipe( gulp.dest( options.coffee.destination ) )
        .pipe( reload( { stream:true } ) );
} );

gulp.task( 'compile:jade', function() {
    // concatenate compiled .jade files into build/templates.js
    gulp.src( options.jade.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.jade().on( 'error', plugins.util.log ) )
        .pipe( plugins.htmlify() )
        .pipe( plugins.tplcache( options.jade.file, {standalone:true}) )
        .pipe( gulp.dest( options.jade.destination ) )
        .pipe( reload( { stream:true } ) );
} );

gulp.task( 'compile:less', function() {
    gulp.src( options.less.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.less( { bare : true } ).on( 'error', plugins.util.log ) )
        .pipe( plugins.concat( options.less.file ) )
        .pipe( gulp.dest( options.less.destination ) )
        .pipe( reload( { stream:true } ) );
} );

gulp.task( 'compile:index', function() {
    gulp.src( options.index.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.jade().on( 'error', plugins.util.log ) )
        .pipe( plugins.htmlify() )
        .pipe( gulp.dest( options.index.destination ) )
        .pipe( reload( { stream:true } ) );
} );

gulp.task( 'compile:libJs', function() {
    gulp.src( options.libjs.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.concat( options.libjs.file ) )
        .pipe( plugins.annotate() )
        .pipe( gulp.dest( options.libjs.destination ) )
        .pipe( reload( { stream:true } ) );
} );

gulp.task( 'compile:libCss', function() {
    gulp.src( options.libcss.files )
        .pipe( plugins.plumber() )
        .pipe( plugins.concat( options.libcss.file ) )
        .pipe( gulp.dest( options.libcss.destination ) )
        .pipe( reload( { stream:true } ) );
} );

// -------------------------------------
//   Test tasks
// -------------------------------------
gulp.task( 'test:css', function() {
    gulp.src( options.css.file )
        .pipe( plugins.plumber() )
        .pipe( plugins.parker() );
});

gulp.task( 'test:js', function() {
    gulp.src( options.js.file )
        .pipe( plugins.plumber() )
        .pipe( plugins.jshint() )
        .pipe( plugins.jshint.reporter( 'default' ) );
});

// -------------------------------------
//   Minify tasks
// -------------------------------------
gulp.task( 'minify:css', function () {
    gulp.src( options.css.file )
        .pipe( plugins.plumber() )
        .pipe( gulp.dest( options.production.destination ) )
        .pipe( plugins.rename( { suffix: '.min' } ) )
        .pipe( plugins.cssmin( { advanced: false } ) )
        .pipe( gulp.dest( options.production.destination ) )
        .pipe( plugins.gzip( gzip_options ) )
        .pipe( gulp.dest( options.production.destination ) );
} );

gulp.task( 'minify:html', function () {
    gulp.src( options.fonts.files )
        .pipe( gulp.dest( options.production.destination + 'font/' ) );
    gulp.src( options.json.files )
        .pipe( gulp.dest( options.production.destination + 'constants/' ) );
    gulp.src( options.html.file )
        .pipe( plugins.plumber() )
        .pipe( gulp.dest( options.production.destination ) )
        .pipe( plugins.rename( { suffix: '.min' } ) )
        .pipe( plugins.htmlmin( { advanced: false } ) )
        .pipe( gulp.dest( options.production.destination ) )
        .pipe( plugins.gzip( gzip_options ) )
        .pipe( gulp.dest( options.production.destination ) );
} );

gulp.task( 'minify:js', function () {
    gulp.src( options.js.file )
        .pipe( plugins.plumber() )
        .pipe( gulp.dest( options.production.destination ) )
        .pipe( plugins.rename( { suffix: '.min' } ) )
        .pipe( plugins.uglify() )
        .pipe( gulp.dest( options.production.destination ) )
        .pipe( plugins.gzip( gzip_options ) )
        .pipe( gulp.dest( options.production.destination ) );
} );

// -------------------------------------
//   Constant and resource tasks
// -------------------------------------
gulp.task( 'fonts', function () {
    gulp.src( options.fonts.files )
        .pipe( gulp.dest( options.fonts.destination ) )
        .pipe( reload( { stream:true } ) );
} );

gulp.task( 'json', function () {
    gulp.src( options.json.files )
        .pipe( gulp.dest( options.json.destination ) )
        .pipe( reload( { stream:true } ) );
} );

// -------------------------------------
//   Watch
// -------------------------------------
gulp.task( 'watch', function() {
    browserSync( { server : {
        baseDir : 'build',
    } } );

    var watchFiles = options.watch.files();

    watchFiles.forEach( function( files, index ) {
        gulp.watch( files, options.watch.run()[ index ]  );
    } );
} );
