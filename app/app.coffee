do ->
  'use strict'

  angular.module('phonebook', [
    'ngSanitize'
    'templates'
    'ui.materialize'
    'ui.router'
    'cb.x2js'
  ])
