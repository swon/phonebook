do ->
  'use strict'

  BookController = ($scope, $http, x2js) ->
    $http.get('phonebook.xml', transformResponse: (data) ->
      x2js = new X2JS
      json = x2js.xml_str2json(data)
      json['SevatecIPPhoneDirectory']['DirectoryEntry']
    ).success (data) ->
      $scope.items = data

  BookController
    .$inject = ['$scope', '$http', 'x2js']

  angular
    .module 'phonebook'
    .controller 'BookController', BookController
