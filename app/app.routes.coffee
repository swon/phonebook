do ->
  'use strict'

  ### @ngInject ###
  config = ($stateProvider, $urlRouterProvider, $locationProvider) ->
    $stateProvider
      .state 'book',
        url: '/'
        templateUrl: 'book/book.html'
        controller: 'BookController as vm'
# @todo Re-instate for future authenticatiohn
#        authenticate: true
#      .state 'login',
#        url: '/login'
#        templateUrl: 'login/login.html'
#        controller: 'LoginController as vm'
#        authenticate: false
    $urlRouterProvider.otherwise '/'
    $locationProvider.html5Mode(true).hashPrefix '!'

  config
    .$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider']

  angular
    .module 'phonebook'
    .config config
